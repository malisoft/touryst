import React, {Component} from 'react';
import {Router,Stack,Scene} from 'react-native-router-flux';

import Login from './pages/Login';
import Signup from './pages/Signup';
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

export default class Routes extends Component<{}>{
  render(){
    return(
      <Router>
        <Stack key="root" hideNavBar={true}>
          <Scene key="Login" component={Login} initial={true} title="Login" />
          <Scene key="Signup" component={Signup} title="Register"/>

        </Stack>
      </Router>
    );
  }
}
