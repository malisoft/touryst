import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,StatusBar,TouchableOpacity} from 'react-native';
import Form from '../components/Form_login';
import { Actions } from 'react-native-router-flux';
import Logo from '../components/Logo';

export default class Login extends Component<Props> {



  render(){

    return(
      <View style={styles.container}>
        <Logo />
        <Form />
        <View style={styles.signupTextCont}>
          <TouchableOpacity onPress={() => Actions.Signup()}><Text style={styles.signupText}>If you don't have an account,SIGN UP</Text></TouchableOpacity>

        </View>
      </View>
    )
  }

}
const styles = StyleSheet.create({
  container: {
    backgroundColor:'#dcdcdc',
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  signupTextCont:{
    flex:1,
    alignItems:'center',
    justifyContent:'flex-end',
    marginVertical:16
  },
  signupText:{
    fontSize:18,
    fontWeight:'500'
  }

});
