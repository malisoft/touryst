import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,StatusBar,Image} from 'react-native';

export default class Logo extends Component<Props> {
  render(){
    return(
      <View style={styles.container}>
        <Image style={{width:200,height:100}}
          source={require('../img/logo.png')}/>
        <Text style={styles.logoText}>"Gezmenin en eğlenceli ve hesaplı yolu"</Text>

      </View>

    )
  }
}

const styles = StyleSheet.create({
  container:{
    flexGrow:0.5,
    justifyContent: 'center',
    alignItems:'center'

  },
  logoText:{
    fontSize:20,
    marginVertical:15,

  }
});
