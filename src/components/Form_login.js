import React, {Component} from 'react';
import {Platform,FlatList, StyleSheet,ActivityIndicator, Text, View,StatusBar,Image,TextInput,TouchableOpacity,AppRegistr} from 'react-native';

export default class Logo extends Component<Props> {
  constructor(){


    super()
    this.state={
      username:'',
      password:'',
      isLoading:false,
      dataa: null,
      data0:null,



    }

  }
  validate(text,field)
  {
    if(field=='username')
    {
      this.setState({
        username:text,

      })
    }
    else if(field=='password')
    {
      this.setState({
        password:text,
      })
    }



  }
  submit()
  {
    this.setState({
      isLoading:true,
    })
    let collection={}
    collection.username=this.state.username,
    collection.password=this.state.password,

    console.log(collection);

    var url = 'https://touryst03.herokuapp.com/api/authentication';

    var data = {email: this.state.username,password:this.state.password};

    fetch(url, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers:{
        'Content-Type': 'application/json',

      }
    }).then(res => res.json())
    .then(response => {
      this.setState({
        isLoading:false,
        data:response.errorMessage,
        data0:response.authentication,
      },function(){

      });

    })

    .catch(error => console.error('Error:', error));

  }







  render(){
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    if(this.state.data0){
      return(
        <View style={{flex: 1, padding: 20}}>
          <Text>LOGGED</Text>
        </View>
      )
    }

    return(
      <View style={styles.container}>
        <TextInput
        style={styles.inputBox}
        underlineColorAndroid='#bfbfbf'
        placeholder="Email"
        placeholderTextColor='#000000'
        onChangeText={(text)=>this.validate(text,'username')}
      />

      <TextInput
        style={styles.inputBox}
        underlineColorAndroid='#bfbfbf'
        placeholder="Password"
        secureTextEntry={true}
        placeholderTextColor='#000000'
        onChangeText={(text)=>this.validate(text,'password')}
      />

      <TouchableOpacity onPress={() => this.submit()} style={styles.button} >
        <Text style={styles.buttonText}>Login</Text>
      </TouchableOpacity>

      <Text>{this.state.data}</Text>
      <Text>{this.state.data0}</Text>

      </View>

    )
  }
}

const styles = StyleSheet.create({
  container:{
    flexGrow:0.3,
    justifyContent: 'center',
    alignItems:'center'

  },
  inputBox:{
    width:315,
    height:50,
    backgroundColor:'#bfbfbf',
    borderRadius:10,
    borderColor:"#696969",
    borderWidth:1,
    paddingHorizontal:15,
    fontSize:20,
    marginVertical:10,
    opacity:20
  },
  button:{
    backgroundColor:'#bfbfbf',
    borderRadius:25,
    width:150,
    marginVertical:10,
    paddingVertical:16

  },
  buttonText:{
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  },
  flatList:{

    flex:0.2
  }

});
