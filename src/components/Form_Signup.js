import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,StatusBar,Image,TextInput,TouchableOpacity,ActivityIndicator,AppRegistr} from 'react-native';
import Modal from 'react-native-modalbox';
export default class Logo extends Component<Props> {
  constructor(){


    super()
    this.state={
      username:'',
      password:'',
      phoneNumber:'',
      name:"",
      surname:"",
      isLoading:null,
      isOpen:false,
      data5:null,

    }


  }
  validate(text,field)
  {
    if(field=='username')
    {
      this.setState({
        username:text,

      })
    }
    else if(field=='password')
    {
      this.setState({
        password:text,
      })
    }
    else if(field=='phonenumber')
    {
      this.setState({
        phonenumber:text,
      })
    }
    else if(field=='name')
    {
      this.setState({
        name:text,
      })
    }
    else if(field=='surname')
    {
      this.setState({
        surname:text,
      })
    }


  }
  submit()
  {
    this.setState({
      isLoading:true,
    })
    let collection={}
    collection.username=this.state.username,
    collection.password=this.state.password,
    collection.phonenumber=this.state.phonenumber,
    collection.name=this.state.name,
    collection.surname=this.state.surname,
    console.log(collection);


    var url = 'https://touryst03.herokuapp.com/api/addUser';

    var data = {email: this.state.username,password:this.state.password,name:this.state.name,surname:this.state.surname,phonenumber:this.state.phonenumber};

    fetch(url, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers:{
        'Content-Type': 'application/json',

      }
    }).then(res => res.json())
    .then(response => {
      this.setState({
        isLoading:false,
        isOpen:true,
        data0:response.errorMessage0,
        data1:response.errorMessage1,
        data2:response.errorMessage2,
        data3:response.errorMessage3,
        data4:response.errorMessage4,
        data5:response.Success,

      },function(){

      });

    })

    .catch(error => console.error('Error:', error));

  }







  render(){
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }
    if(this.state.data5){
      return(
        <View style={{flex: 1, padding: 20}}>
          <Text>LOGGED</Text>
        </View>
      )
    }
    return(
      <View style={styles.container}>
        <TextInput
        style={styles.inputBox}
        underlineColorAndroid='#bfbfbf'
        placeholder="Email"
        placeholderTextColor='#000000'
        onChangeText={(text)=>this.validate(text,'username')}
      />

      <TextInput
        style={styles.inputBox}
        underlineColorAndroid='#bfbfbf'
        placeholder="Password"
        secureTextEntry={true}
        placeholderTextColor='#000000'
        onChangeText={(text)=>this.validate(text,'password')}
      />
      <TextInput
        style={styles.inputBox}
        underlineColorAndroid='#bfbfbf'
        placeholder="Phone Number"

        placeholderTextColor='#000000'
        onChangeText={(text)=>this.validate(text,'phonenumber')}
      />
      <TextInput
        style={styles.inputBox}
        underlineColorAndroid='#bfbfbf'
        placeholder="Name "

        placeholderTextColor='#000000'
        onChangeText={(text)=>this.validate(text,'name')}
      />
      <TextInput
        style={styles.inputBox}
        underlineColorAndroid='#bfbfbf'
        placeholder="Surname "

        placeholderTextColor='#000000'
        onChangeText={(text)=>this.validate(text,'surname')}
      />

      <TouchableOpacity onPress={() => this.submit()} style={styles.button}>
        <Text style={styles.buttonText}>SignUp</Text>
      </TouchableOpacity>
      <Text>{this.state.data0}</Text>
      <Text>{this.state.data1}</Text>
      <Text>{this.state.data2}</Text>
      <Text>{this.state.data3}</Text>
      <Text>{this.state.data4}</Text>



      </View>

    )
  }
}

const styles = StyleSheet.create({
  container:{
    flexGrow:0.5,
    justifyContent: 'center',
    alignItems:'center'

  },
  inputBox:{
    width:315,
    height:50,
    backgroundColor:'#bfbfbf',
    borderRadius:10,
    borderColor:"#696969",
    borderWidth:1,
    paddingHorizontal:20,
    fontSize:20,
    marginVertical:10,
    opacity:20
  },
  button:{
    backgroundColor:'#bfbfbf',
    borderRadius:25,
    width:150,
    marginVertical:10,
    paddingVertical:16

  },
  buttonText:{
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  }

});
